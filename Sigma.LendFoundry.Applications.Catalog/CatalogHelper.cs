﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog
{
    /// <summary>
    /// Contains helper methods pertaining to catalog management.
    /// </summary>
    public class CatalogHelper
    {
        /// <summary>
        /// Given a name, this method generates an Id in the following format: all lower case, spaces replaced by dashes
        /// example : "Is Valid Score" -> is-valid-score 
        /// </summary>
        public static string GenerateIdFromName(string name)
        {
            string id = name.Trim().ToLower();
            id = id.Replace(' ', '-');

            while (id.Contains("--"))
            {
                id = id.Replace("--", "-");
            }

            return id;
        }
    }
}
