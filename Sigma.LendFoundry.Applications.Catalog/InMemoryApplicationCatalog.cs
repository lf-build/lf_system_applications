﻿using Sigma.LendFoundry.Applications.Catalog.Auth;
using Sigma.LendFoundry.Applications.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog
{
    public class InMemoryApplicationCatalog : ICatalog<Application>
    {
        private static Dictionary<string, Application> Catalog = new Dictionary<string, Application>();

        private string GetToken(Application application)
        {
            TokenProvider tokenProvider = new TokenProvider();
            string token = tokenProvider.GetToken(application);

            return token;
        }


        public string Register(Application application)
        {
            string id = CatalogHelper.GenerateIdFromName(application.Name);
            application.UserFriendlyId = id;

            string token = GetToken(application);
            application.Token = token;

            if(Catalog.ContainsKey(id))
            {
                throw new Exception("An application with this Id already exists.");
            }

            Catalog.Add(id, application);
            return id;
        }

        public void Activate(Application application)
        {
            string id = application.UserFriendlyId;
            if(Catalog.TryGetValue(id, out application))
            {
                application.IsActive = true;
                return;
            }

            throw new Exception("Not found");
        }

        public void Deactivate(Application application)
        {
            string id = application.UserFriendlyId;
            if (Catalog.TryGetValue(id, out application))
            {
                application.IsActive = false;
                return;
            }

            throw new Exception("Not found");
        }

        public Application Get(Application application)
        {
            string id = application.UserFriendlyId;
            if (Catalog.TryGetValue(id, out application))
            {
                return application;
            }

            throw new Exception("Not found");
        }        

        public void Delete(Application application)
        {
            string id = application.UserFriendlyId;

            if (Catalog.TryGetValue(id, out application))
            {
                Catalog.Remove(id);
                return;
            }

            throw new Exception("Not found");
        }

        public Application ResetApplicationSecret(Application application)
        {
            string id = application.UserFriendlyId;

            if (Catalog.TryGetValue(id, out application))
            {
                application.AppKey = "NewApplicationSecret";
                string token = GetToken(application);
                application.Token = token;

                return application;
            }

            throw new Exception("Not found");
        }
    }
}