﻿using MySql.Data.Entity;
using MySql.Data.MySqlClient;
using Sigma.LendFoundry.Applications.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations.History;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog
{
    [DbConfigurationType(typeof(ApplicationsDbConfig))]
    public class ApplicationsDbContext : DbContext
    {
        public virtual DbSet<Application> Applications { get; set; }


        public ApplicationsDbContext(string connectionString) : base(connectionString)
        {
            //Database.SetInitializer<ApplicationsDbContext>(new CreateDatabaseIfNotExists<ApplicationsDbContext>());
            //Database.SetInitializer<ApplicationsDbContext>(new DropCreateDatabaseIfModelChanges<ApplicationsDbContext>());
            Database.SetInitializer<ApplicationsDbContext>(new DropCreateDatabaseAlways<ApplicationsDbContext>());

            Applications = Set<Application>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new ApplicationConfigurationMap());
        }
    }
}
