﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog
{
    public interface ICatalog<Application>
    {
        string Register(Application application);
        //void Activate(string id);
        //void Deactivate(string id);
        void Activate(Application application);
        void Deactivate(Application application);

        //Application Get(string id);
        Application Get(Application application);

        //void Delete(string id);
        void Delete(Application application);
        Application ResetApplicationSecret(Application application);
    }
}
