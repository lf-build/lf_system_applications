﻿using Sigma.LendFoundry.Applications.Catalog.Auth;
using Sigma.LendFoundry.Applications.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog
{
    public class ApplicationCatalog : ICatalog<Application>
    {
        private ApplicationsDbContext Context { get; set; }

        public ApplicationCatalog()
        {
            string connectionString = "Server=localhost;Database=lfapplication1;Uid=girish_a;Pwd=test;";
            Context = new ApplicationsDbContext(connectionString);
        }

        public string GenerateAppKey()
        {
            string guid = Guid.NewGuid().ToString();
            return guid;
        }

        private string GetToken(Application application)
        {
            TokenProvider tokenProvider = new TokenProvider();
            string token = tokenProvider.GetToken(application);

            return token;
        }

        public void Activate(Application application)
        {
            var app = Context.Applications.FirstOrDefault(a => a.UserFriendlyId == application.UserFriendlyId && a.TenantId == application.TenantId);

            if(app != null)
            {
                app.IsActive = true;
                Context.Entry(app).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Deactivate(Application application)
        {
            var app = Context.Applications.FirstOrDefault(a => a.UserFriendlyId == application.UserFriendlyId && a.TenantId == application.TenantId);

            if (app != null)
            {
                app.IsActive = false;
                Context.Entry(app).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(Application application)
        {
            Context.Entry(application).State = System.Data.Entity.EntityState.Deleted;
            Context.Applications.Remove(application);
            Context.SaveChanges();
        }

        public Application Get(Application application)
        {
            var result = Context.Applications.FirstOrDefault(a => a.UserFriendlyId == application.UserFriendlyId && a.TenantId == application.TenantId);
            // var result = (Application)Context.Applications.Find(application.Id, application.TenantId);
            return result;
        }

        public string Register(Application application)
        {
            string id = CatalogHelper.GenerateIdFromName(application.Name);
            application.UserFriendlyId = id;

            string appKey = GenerateAppKey();
            application.AppKey = appKey;

            string token = GetToken(application);
            application.Token = token;

            Context.Applications.Add(application);

            // There is a known MySql bug that can strike here. Se the SO question below. I was able to replicate this error.
            // Right now, there is no fix.
            // http://stackoverflow.com/questions/21442928/how-to-avoid-a-nested-transactions-are-not-supported-error
            // http://bugs.mysql.com/bug.php?id=71502
            Context.SaveChanges();

            return id;
        }

        public Application ResetApplicationSecret(Application application)
        {
            string appKey = GenerateAppKey();
            application.AppKey = appKey;

            string token = GetToken(application);
            application.Token = token;

            Context.SaveChanges();

            return application;
        }
    }
}
