﻿using MySql.Data.Entity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog
{
    public class ApplicationsDbConfig : MySqlEFConfiguration
    {
        public ApplicationsDbConfig()
        {
            var dataSet = (DataSet)ConfigurationManager.GetSection("system.data");

            if (dataSet.Tables[0].Rows != null && dataSet.Tables[0].Rows.Count > 0)
            {
                dataSet.Tables[0].Rows.Clear();
            }

            dataSet.Tables[0].Rows.Add(
                "MySQL Data Provider",
                ".Net Framework Data Provider for MySQL",
                "MySql.Data.MySqlClient",
                typeof(MySqlClientFactory).AssemblyQualifiedName
            );

            SetProviderServices("MySql.Data.MySqlClient", new MySqlProviderServices());
            SetDefaultConnectionFactory(new MySql.Data.Entity.MySqlConnectionFactory());
            SetHistoryContext("MySql.Data.MySqlClient", (conn, schema) => new MySqlHistoryContext(conn, schema));
        }
    }
}
