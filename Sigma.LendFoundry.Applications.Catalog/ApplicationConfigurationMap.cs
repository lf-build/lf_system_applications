﻿using MySql.Data.Entity;
using Sigma.LendFoundry.Applications.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.Entity.Migrations.History;

namespace Sigma.LendFoundry.Applications.Catalog
{
    public class ApplicationConfigurationMap : EntityTypeConfiguration<Application>
    {
        public ApplicationConfigurationMap()
        {
            this.ToTable("Application");
            //this.HasKey(t => new { t.TenantId, t.UserFriendlyId });
        }
    }
}