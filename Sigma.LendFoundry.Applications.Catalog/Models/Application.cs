﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog.Models
{
    public class Application
    {
        //[Index(IsClustered = false, IsUnique = true)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Key]
        [Column(Order = 1)]
        public string TenantId { get; set; }
        [Key]
        [Column(Order = 2)]
        public string UserFriendlyId { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string AppKey { get; set; }
        public bool IsActive { get; set; }
    }
}
