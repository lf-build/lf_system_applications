﻿using Sigma.LendFoundry.Applications.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog.Auth
{
    public class TokenProvider
    {
        public string GetToken(Application application)
        {
            ITokenGenerator tokenGenerator = new JoseJwtGenerator();
            string token = tokenGenerator.GenerateToken(application);
            return token;
        }
    }
}
