﻿using Sigma.LendFoundry.Applications.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog.Auth
{
    public interface ITokenGenerator
    {
        string GenerateToken(Application application);
    }
}
