﻿using Jose;
using Sigma.LendFoundry.Applications.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Catalog.Auth
{
    public class JoseJwtGenerator : ITokenGenerator
    {
        public string GenerateToken(Application application)
        {
            // application.ApplicationSecret = "AppSecret";

            // string appKey = GenerateAppKey();

            if(string.IsNullOrWhiteSpace(application.AppKey))
            {
                throw new Exception("Could not generate token. Application key was not provided.");
            }

            var payload = new Dictionary<string, object>()
            {
                { "tenantId", application.TenantId },
                { "appKey", application.AppKey }
            };

            string secret = GetSigningSecretFromConfig();

            byte[] secretKey = Encoding.UTF8.GetBytes(secret);
            string token = JWT.Encode(payload, secretKey, JwsAlgorithm.HS256);
            return token;
        }


        private string GetSigningSecretFromConfig()
        {
            return "signing secret";
        }
    }
}
