﻿using AutoMapper;
using Sigma.LendFoundry.Applications.Catalog.Models;
using Sigma.LendFoundry.Applications.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Api.Mappings
{
    public class MappingEngine : IMappingEngine
    {
        public void CreateMappings()
        {
            Mapper.CreateMap<RegisterApplicationViewModel, Application>();
            Mapper.CreateMap<Application, RegisterApplicationViewModel>();
        }
    }
}
