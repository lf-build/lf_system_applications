﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Api.Mappings
{
    public class MappingEngineProvider
    {
        public static IMappingEngine GetMappingEngine()
        {
            return new MappingEngine();
        }
    }
}
