﻿using Microsoft.AspNet.Mvc;
using Sigma.LendFoundry.Applications.Catalog.Models;
using Sigma.LendFoundry.Applications.Api.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Api.Filters
{
    /// <summary>
    /// This action filter is used to extract tenant information that comes in to the Web API via request headers to controller
    /// actions and populate the base controller's Tenant property.
    /// </summary>
    public class PopulateTenantInfoAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (actionContext.Controller is BaseController)
            {
                string[] keys = null;

                // We need to populate tenant info here
                if (!actionContext.HttpContext.Request.Headers.TryGetValue("Tenant", out keys))
                {
                    throw new Exception("Tenant data not available.");
                }
                //else if(!actionContext.HttpContext.Request.Headers.TryGetValue("AuthToken", out keys))
                //{
                //    throw new Exception("Authorization token was not supplied.");
                //}

                string tenantId = keys.First();

                var controller = (BaseController)actionContext.Controller;
                controller.Tenant = new Tenant();
                controller.Tenant.TenantId = tenantId;
            }
        }
    }
}
