﻿using Microsoft.AspNet.Mvc;
using Sigma.LendFoundry.Applications.Catalog.Models;
using Sigma.LendFoundry.Applications.Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Api.Controllers
{
    /// <summary>
    /// Base controller which other API controllers can implement. The advantage of deriving from this base controller is that
    /// this controller can hold properties that can be auto-populated via action filters at runtime before the controller action
    /// is invoked by the Web API framework.
    /// </summary>
    [PopulateTenantInfo]
    public class BaseController : Controller
    {
        /// <summary>
        /// The tenant for which a request is submitted. This property is auto-populated from the request header by the filter
        /// which processes request headers for extracting tenant info.
        /// </summary>
        public Tenant Tenant { get; set; }
    }
}
