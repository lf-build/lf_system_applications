﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Sigma.LendFoundry.Applications.Catalog.Models;
using Sigma.LendFoundry.Applications.Api.ViewModels;
using Sigma.LendFoundry.Applications.Catalog;
using AutoMapper;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma.LendFoundry.Applications.Api.Controllers
{
    [Route("api/[controller]")]
    public class ApplicationController : BaseController
    {
        private ICatalog<Application> Catalog { get; set; }

        public ApplicationController(ICatalog<Application> catalog)
        {
            Catalog = catalog;
        }

        [HttpPut]
        public IActionResult RegisterApplication([FromBody]RegisterApplicationViewModel applicationVm)
        {
            var application = Mapper.Map<Application>(applicationVm);

            application.TenantId = Tenant.TenantId;
            string id = Catalog.Register(application);

            return new ObjectResult(id);
        }

        [HttpPost]
        [Route("{id}/activate")]
        public IActionResult ActivateApplication([FromRoute]string id)
        {
            Application application = new Application();
            application.UserFriendlyId = id;
            application.TenantId = Tenant.TenantId;


            Catalog.Activate(application);
            return new ObjectResult(Convert.ToInt32(HttpStatusCode.OK));
        }

        [HttpPost]
        [Route("{id}/deactivate")]
        public IActionResult DeactivateApplication([FromRoute]string id)
        {
            Application application = new Application();
            application.UserFriendlyId = id;
            application.TenantId = Tenant.TenantId;



            Catalog.Deactivate(application);
            return new ObjectResult(Convert.ToInt32(HttpStatusCode.OK));
        }

        [HttpDelete]
        [Route("{id}/delete")]
        public IActionResult DeleteApplication([FromRoute]string id)
        {
            Application application = new Application();
            application.UserFriendlyId = id;
            application.TenantId = Tenant.TenantId;

            Catalog.Delete(application);
            return new ObjectResult(Convert.ToInt32(HttpStatusCode.OK));
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetApplicationDetails([FromRoute]string id)
        {
            Application application = new Application();
            application.UserFriendlyId = id;
            application.TenantId = Tenant.TenantId;

            // TODO: Seems unintuitive, getting an application object back by passing in an application
            // Use a different object for the combination of tenant Id and application Id
            var result = Catalog.Get(application);
            return new ObjectResult(result);
        }

        [HttpPost]
        [Route("{id}/reset")]
        public IActionResult ResetApplicationSecret([FromRoute]string id)
        {
            Application application = new Application();
            application.UserFriendlyId = id;
            application.TenantId = Tenant.TenantId;

            var result = Catalog.ResetApplicationSecret(application);
            return new ObjectResult(application);
        }
    }
}