﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Applications.Api.ViewModels
{
    public class RegisterApplicationViewModel
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string ApplicationSecret { get; set; }
        public bool IsActive { get; set; }
    }
}
