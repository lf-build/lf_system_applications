﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Routing;
using Microsoft.Framework.DependencyInjection;
using Sigma.LendFoundry.Applications.Catalog;
using Sigma.LendFoundry.Applications.Catalog.Models;
using Sigma.LendFoundry.Applications.Api.Mappings;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.Runtime;
using System.Configuration;

namespace Sigma.LendFoundry.Applications.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IHostingEnvironment env, IApplicationEnvironment appEnv)
        {
            // Setup configuration sources.
            var builder = new ConfigurationBuilder(appEnv.ApplicationBasePath)
                .AddJsonFile("config.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        //public Startup(IHostingEnvironment env)
        //{
        //}

        // This method gets called by a runtime.
        // Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //services.AddSingleton<IConfiguration>(_ => Configuration);
            //services.AddScoped<ApplicationsDbContext>();

            services.AddScoped<ICatalog<Application>, ApplicationCatalog>();
            // services.AddScoped<ICatalog<Application>, InMemoryApplicationCatalog>();

            CreateMappings();

            // Uncomment the following line to add Web API services which makes it easier to port Web API 2 controllers.
            // You will also need to add the Microsoft.AspNet.Mvc.WebApiCompatShim package to the 'dependencies' section of project.json.
            // services.AddWebApiConventions();
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Configure the HTTP request pipeline.
            app.UseStaticFiles();

            // Add MVC to the request pipeline.
            app.UseMvc();
            // Add the following route for porting Web API 2 controllers.
            // routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");
        }

        private void CreateMappings()
        {
            MappingEngineProvider.GetMappingEngine().CreateMappings();
        }
    }
}
